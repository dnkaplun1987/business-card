"use client"

import { I18nextProvider } from "react-i18next"
import initTranslations from "@/i18n"
import React, { FC, memo, PropsWithChildren, useEffect, useState } from "react"
import { i18n } from "i18next"

interface Props {
  namespaces: string[]
  locale: string
}

/**
 * The wrapper for translation provider
 * @param props
 * @returns
 */
const TranslationsProvider: FC<PropsWithChildren<Props>> = props => {
  const { children, locale, namespaces } = props

  const [instance, setInstance] = useState<i18n | undefined>(undefined)

  //  Initialize i18n instance
  const initInstance = async () => {

    const newInstance = await initTranslations(locale, namespaces)

    setInstance(newInstance)
  }

  useEffect(() => {
    initInstance()
  }, [])

  //  the effect changes language if it needs
  useEffect(() => {
    if (!instance) {
      return
    }
    
    if (instance.language === locale) {
      return
    }

    instance.changeLanguage(locale)
  }, [locale, namespaces, instance])

  if (!instance) {
    return null;
  }

  return (
    <I18nextProvider i18n={instance} defaultNS={namespaces[0]}>
      {children}
    </I18nextProvider>
  );
}

export default memo(TranslationsProvider)
