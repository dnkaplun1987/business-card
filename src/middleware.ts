import { NextRequest, NextResponse } from 'next/server'
import { i18n } from '../i18n-config';

export function middleware(req: NextRequest) {
  const { pathname } = req.nextUrl

  //  redirect empty pathname to default language
  if (!pathname.length || pathname === "/") {
    return NextResponse.redirect(new URL("/ru", req.url));
  }

  //  add the default language if the first path is incorrect
  if (i18n.locales.every(locale => pathname.split("/")[1] !== locale)) {
    return NextResponse.redirect(new URL(`/ru${pathname}`, req.url));
  }
  
  return NextResponse.next();
}

export const config = {
  matcher: ['/((?!api|_next|_vercel|.*\\..*).*)']
}