import { redirect } from "next/navigation"

//  If route didn't find redirect on the main page
const NotFound = () => redirect("/ru")

export default NotFound