"use client"

import { CssBaseline, ThemeOptions, ThemeProvider, createTheme } from "@mui/material";
import React, { FC, PropsWithChildren, useMemo } from "react"
import breakpoints from "@/theme/breakpoints"

/**
 * The wrapper for theme provider
 * @param param0
 * @returns
 */
const ThemeWrapper: FC<PropsWithChildren> = ({ children }) => {
  const themeOptions: ThemeOptions = useMemo(
    () => ({
      breakpoints,
      shape: { borderRadius: 8 },
    }),
    []
  )

  const theme = createTheme(themeOptions)

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />

        {children}
      </ThemeProvider>
    </>
  );
}

export default ThemeWrapper
