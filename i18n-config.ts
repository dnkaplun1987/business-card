export const i18n = {
  defaultLocale: 'ru',
  locales: ['en', 'ru'] as [string, string],
  routingStrategy: 'dynamicSegment',
} as const

export type Locale = (typeof i18n)['locales'][number]