import { TranslationsProvider } from "./_components/TranslationsProvider"
import initTranslations from "../../i18n"
import { ThemeWrapper } from "@/theme"

interface Props {
  params: { locale: string }
}

const AppPage = async ({ params: { locale } }: Props) => {
  const { t, options } = await initTranslations(locale, ["test"])

  return (
    <TranslationsProvider namespaces={options.ns as string[]} locale={locale}>
      <ThemeWrapper>
        <header>

        </header>

        <main>
          
        </main>

        <footer>
          
        </footer>
      </ThemeWrapper>
    </TranslationsProvider>
  );
}

export default AppPage