import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { FC, PropsWithChildren } from "react";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Business card",
  description: "Dmitry Kaplun's business card website",
};

/**
 * The root layout component
 * @param param0
 * @returns
 */
const RootLayout: FC<PropsWithChildren> = ({
  children,
}) => {
  return (
    <html lang="en">
      <body className={inter.className}>
        {children}
      </body>
    </html>
  );
};

export default RootLayout;
